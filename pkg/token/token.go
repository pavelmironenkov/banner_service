package token

import (
	"banner_service/pkg/myerrors"
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type sessKey string

var (
	key                = []byte("osfhvjfblkvbke")
	sessionKey sessKey = "sessionKey"
)

const (
	Admin = "admin"
	User  = "user"
)

func RoleFromContext(ctx context.Context) (*string, error) {
	role, ok := ctx.Value(sessionKey).(*string)
	if !ok || role == nil {
		return nil, myerrors.ErrNoAuth
	}
	return role, nil
}

func ContextWithRole(ctx context.Context, role string) context.Context {
	return context.WithValue(ctx, sessionKey, role)
}

func Create(role string) (string, error) {
	if role != "admin" && role != "user" {
		return "", fmt.Errorf("can't create token")
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"role": role,
		"iat":  time.Now().Unix(),
		"exp":  time.Now().Add(30 * 24 * time.Hour).Unix(),
	})
	return token.SignedString(key)
}

func Check(tokenString string) (*string, error) {
	_, tokenString, ok := strings.Cut(tokenString, "Bearer ")
	if !ok {
		return nil, myerrors.ErrNoAuth
	}

	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(tokenString, claims, func(t *jwt.Token) (interface{}, error) {
		if t.Method != jwt.SigningMethodHS256 {
			return nil, fmt.Errorf("couldn't parse token")
		}
		return key, nil
	})
	if err != nil {
		return nil, myerrors.ErrNoAuth
	}
	role, ok := (claims["role"]).(string)
	if !ok {
		return nil, myerrors.ErrConvIface
	}

	return &role, nil
}
