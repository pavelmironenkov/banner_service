package myerrors

import (
	"encoding/json"
	"errors"
	"net/http"
)

var (
	ErrBadDBTransaction  = errors.New("can't create db transaction")
	ErrBadScanBanner     = errors.New("failed to scan banners")
	ErrNoBanner          = errors.New("banner not found")
	ErrBadDataBase       = errors.New("internal db error")
	ErrCantCreateBanners = errors.New("banner could not be created")
	ErrUpdateBanner      = errors.New("failed to update banner")
	ErrUpdateLog         = errors.New("failed to update log")
	ErrInsertLog         = errors.New("failed to insert new log")
	ErrDeleteLog         = errors.New("failed when trying to place the log deleted")
	ErrInvalidRole       = errors.New("invalid role")
	ErrNoAuth            = errors.New("no session found")
	ErrConvIface         = errors.New("hasn't convert interfaces")
)

func HandleMarshalError(w http.ResponseWriter, v any) bool {
	resp, err := json.Marshal(v)
	if err != nil {
		http.Error(w, "Marshalling err", http.StatusInternalServerError)

		return false
	}
	_, err = w.Write(resp)
	if err != nil {
		http.Error(w, "Writing response err", http.StatusInternalServerError)

		return false
	}

	return true
}
