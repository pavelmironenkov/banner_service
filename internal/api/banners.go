package banners

import (
	"time"
)

type Content struct {
	Title string `json:"title"`
	Text  string `json:"text"`
	URL   string `json:"url"`
}

type Banner struct {
	BannerID  int       `json:"banner_id"  valid:"-"`
	Content   Content   `json:"content"    valid:"required"`
	IsActive  bool      `json:"is_active"  valid:"required"`
	Status    string    `json:"-"          valid:"-"`
	FeatureID int       `json:"feature_id" valid:"required"`
	TagIDs    []int     `json:"tag_ids"    valid:"required"`
	CreatedAt time.Time `json:"created_at" valid:"-"`
	UpdatedAt time.Time `json:"updated_at" valid:"-"`
}
