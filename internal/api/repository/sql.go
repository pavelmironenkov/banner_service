package repository

const getContextFromBannerForAdmin = `
SELECT 
    title, text, url 
FROM banners AS b 
    INNER JOIN log AS l ON l.banner_id = b.banner_id  
WHERE feature_id = $1 AND tag_id = $2 AND status != 'deleted'
`

const getContextFromBannerForUser = `
SELECT 
    title, text, url 
FROM banners AS b 
    INNER JOIN log AS l ON l.banner_id = b.banner_id  
WHERE feature_id = $1 AND tag_id = $2 AND is_active = true AND status != 'deleted'
`

const getBannersWithFilter = `
SELECT
	banner_id, title, text, url, is_active, feature_id, array_agg(bt.tag_id) as tag_ids, created_at, updated_at
FROM banners AS b
	INNER JOIN log AS l ON l.banner_id = b.banner_id 
	                                    	AND (NULLIF($1, '') IS NULL OR tag_id = $1 ) 
	INNER JOIN log ON l.banner_id = b.banner_id
WHERE (NULLIF($2, '') IS NULL OR feature_id = $2) AND status != 'deleted'
  -- AND (NULLIF($2, '') IS NULL OR tag_id = $2 )
GROUP BY banner_id, title, text, url, is_active, feature_id, created_at, updated_at
ORDER BY updated_at DESC
LIMIT NULLIF($3, '') OFFSET NULLIF($4, '')
`

const insertNewBanner = `
INSERT INTO banners (title, text, url, is_active) 
VALUES ($1, $2, $3, $4) 
RETURNING banner_id
`

const insertNewLog = `
INSERT INTO log (tag_id, banner_id, feature_id) VALUES %s 
ON CONFLICT( tag_id, banner_id, feature_id) DO UPDATE SET status = 'updated' 
`

const patchBanners = `
UPDATE banners (title, text, url, is_active) SET ($1, $2, $3, $4) WHERE banner_id = $5 
`

const patchLogStatus = `
UPDATE log (status) SET $1 WHERE banner_id = $2
`

const getLastUpdatedBanners = `
SELECT title, text, url, is_active, tag_id, feature_id, status 
FROM banners AS b
	INNER JOIN log AS l ON l.banner_id = b.banner_id
WHERE updated_at >= DATEADD(minute,-5,GETDATE()) 
ORDER BY updated_at DESC
`
