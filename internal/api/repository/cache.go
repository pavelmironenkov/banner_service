package repository

import (
	banners "banner_service/internal/api"
	"banner_service/pkg/myerrors"

	lru "github.com/hashicorp/golang-lru/v2"
)

const CacheSize = 1000

type CacheRepo interface {
	GetBannerForUser(tagID, featureID int) (banners.Content, error)
	AddOrUpdateBannerToCache(tagID, featureID int, banner banners.Content)
	RemoveBannerFromCache(tagID, featureID int) error
}

type KeyRepositoryCache struct {
	tagID     int
	featureID int
}

type Cache struct {
	cache *lru.Cache[KeyRepositoryCache, banners.Content]
}

func NewCache(cache *lru.Cache[KeyRepositoryCache, banners.Content]) *Cache {
	return &Cache{
		cache: cache,
	}
}

func (c *Cache) GetBannerForUser(tagID, featureID int) (banners.Content, error) {
	content, ok := c.cache.Get(KeyRepositoryCache{tagID: tagID, featureID: featureID})
	if !ok {
		return banners.Content{}, myerrors.ErrNoBanner
	}
	return content, nil
}

func (c *Cache) AddOrUpdateBannerToCache(tagID, featureID int, banner banners.Content) {
	c.cache.Add(KeyRepositoryCache{tagID: tagID, featureID: featureID}, banner)
}

func (c *Cache) RemoveBannerFromCache(tagID, featureID int) error {
	isContains := c.cache.Remove(KeyRepositoryCache{tagID: tagID, featureID: featureID})
	if !isContains {
		return myerrors.ErrNoBanner
	}
	return nil
}
