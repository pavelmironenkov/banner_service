package repository

import (
	"banner_service/pkg/myerrors"
	"context"
	"database/sql"
	"fmt"
	"strings"

	banners "banner_service/internal/api"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

type BannersRepo interface {
	GetBannerForUser(ctx context.Context, tagID, featureID int) (banners.Content, error)
	GetBannerForAdmin(ctx context.Context, tagID, featureID int) (banners.Content, error)
	FilterBanners(ctx context.Context, tagID, featureID, offset, limit string) ([]banners.Banner, error)
	CreateBanner(ctx context.Context, banner banners.Banner) (int, error)
	PatchBanner(ctx context.Context, banner banners.Banner, bannerID int) error
	DeleteBanner(ctx context.Context, bannerID int) error
	GetLastUpdatedBanners(ctx context.Context) ([]banners.Banner, error)
}

type BannersSQL struct {
	db *sql.DB
}

func NewPostgresRepo(db *sql.DB) *BannersSQL {
	return &BannersSQL{
		db: db,
	}
}

func (repo *BannersSQL) GetBannerForUser(ctx context.Context, tagID, featureID int) (banners.Content, error) {
	contextFromBanner := banners.Content{}
	tr, err := repo.db.BeginTx(ctx, &sql.TxOptions{ReadOnly: true})
	defer tr.Rollback()
	if err != nil {
		return banners.Content{}, myerrors.ErrBadDBTransaction
	}
	row := tr.QueryRowContext(ctx, getContextFromBannerForUser, featureID, tagID)
	err = row.Scan(&contextFromBanner.Title, &contextFromBanner.Text, &contextFromBanner.URL)
	if errors.Is(err, sql.ErrNoRows) {
		return banners.Content{}, myerrors.ErrNoBanner
	}
	if err != nil {
		return banners.Content{}, myerrors.ErrBadDataBase
	}
	tr.Commit()
	return contextFromBanner, nil
}

func (repo *BannersSQL) GetBannerForAdmin(ctx context.Context, tagID, featureID int) (banners.Content, error) {
	contextFromBanner := banners.Content{}
	tr, err := repo.db.BeginTx(ctx, &sql.TxOptions{ReadOnly: true})
	defer tr.Rollback()
	if err != nil {
		return banners.Content{}, myerrors.ErrBadDBTransaction
	}
	row := tr.QueryRowContext(ctx, getContextFromBannerForAdmin, featureID, tagID)
	err = row.Scan(&contextFromBanner.Title, &contextFromBanner.Text, &contextFromBanner.URL)
	if errors.Is(err, sql.ErrNoRows) {
		return banners.Content{}, myerrors.ErrNoBanner
	}
	if err != nil {
		return banners.Content{}, myerrors.ErrBadDataBase
	}
	tr.Commit()
	return contextFromBanner, nil
}

func (repo *BannersSQL) FilterBanners(ctx context.Context,
	tagID, featureID, offset, limit string,
) ([]banners.Banner, error) {
	tr, err := repo.db.BeginTx(ctx, &sql.TxOptions{ReadOnly: true})
	defer tr.Rollback()
	if err != nil {
		return nil, myerrors.ErrBadDBTransaction
	}
	var filteringBanners []banners.Banner
	rows, err := tr.QueryContext(ctx, getBannersWithFilter, tagID, featureID, limit, offset)
	if err != nil {
		return nil, errors.Wrap(rows.Err(), myerrors.ErrBadDataBase.Error())
	}
	defer rows.Close()
	var banner banners.Banner
	for rows.Next() {
		err := rows.Scan(&banner.BannerID,
			&banner.Content.Title,
			&banner.Content.Text,
			&banner.Content.URL,
			&banner.IsActive,
			&banner.FeatureID,
			&banner.TagIDs,
			&banner.CreatedAt,
			&banner.UpdatedAt)
		if err != nil {
			return nil, myerrors.ErrBadScanBanner
		}
		filteringBanners = append(filteringBanners, banner)
	}
	return filteringBanners, nil
}

func (repo *BannersSQL) CreateBanner(ctx context.Context, banner banners.Banner) (int, error) {
	tr, err := repo.db.BeginTx(ctx, &sql.TxOptions{})
	defer tr.Rollback()
	if err != nil {
		return -1, myerrors.ErrBadDBTransaction
	}
	var bannerID int
	row := tr.QueryRowContext(ctx, insertNewBanner,
		banner.Content.Title,
		banner.Content.Text,
		banner.Content.URL,
		banner.IsActive)
	err = row.Scan(&bannerID)
	if errors.Is(err, sql.ErrNoRows) {
		return -1, myerrors.ErrCantCreateBanners
	}
	if err != nil {
		return -1, myerrors.ErrBadDataBase
	}

	query, args := repo.bulkInsert(banner.TagIDs, bannerID, banner.FeatureID)
	_, err = tr.ExecContext(ctx, query, args...)
	if err != nil {
		return -1, myerrors.ErrInsertLog
	}
	tr.Commit() // no
	return bannerID, nil
}

func (repo *BannersSQL) PatchBanner(ctx context.Context, banner banners.Banner, bannerID int) error {
	tr, err := repo.db.BeginTx(ctx, &sql.TxOptions{})
	defer tr.Rollback()
	if err != nil {
		return myerrors.ErrBadDBTransaction
	}

	_, err = tr.ExecContext(ctx, patchBanners,
		banner.Content.Title,
		banner.Content.Text,
		banner.Content.URL,
		banner.IsActive,
		bannerID)
	if err != nil {
		return myerrors.ErrUpdateBanner
	}
	_, err = tr.ExecContext(ctx, patchLogStatus,
		"deleted",
		bannerID)
	if err != nil {
		return myerrors.ErrUpdateBanner
	}

	query, args := repo.bulkInsert(banner.TagIDs, bannerID, banner.FeatureID)

	_, err = tr.ExecContext(ctx, query, args...)
	if err != nil {
		return myerrors.ErrUpdateLog
	}

	tr.Commit()
	return nil
}

func (repo *BannersSQL) DeleteBanner(ctx context.Context, bannerID int) error {
	tr, err := repo.db.BeginTx(ctx, &sql.TxOptions{ReadOnly: false})
	defer tr.Rollback()
	if err != nil {
		return myerrors.ErrBadDBTransaction
	}
	_, err = tr.ExecContext(ctx, patchLogStatus,
		"deleted",
		bannerID)
	if err != nil {
		return myerrors.ErrDeleteLog
	}
	tr.Commit()
	return nil
}

func (repo *BannersSQL) GetLastUpdatedBanners(ctx context.Context) ([]banners.Banner, error) {
	tr, err := repo.db.BeginTx(ctx, &sql.TxOptions{})
	defer tr.Rollback()
	if err != nil {
		return nil, myerrors.ErrBadDBTransaction
	}
	var Banners []banners.Banner
	rows, err := tr.QueryContext(ctx, getLastUpdatedBanners)
	if err != nil {
		return nil, errors.Wrap(rows.Err(), myerrors.ErrBadDataBase.Error())
	}
	defer rows.Close()
	var banner banners.Banner
	for rows.Next() {
		var tagID int
		err := rows.Scan(
			&banner.Content.Title,
			&banner.Content.Text,
			&banner.Content.URL,
			&banner.IsActive,
			&tagID,
			&banner.FeatureID,
			&banner.Status)
		if err != nil {
			return nil, myerrors.ErrBadScanBanner
		}
		banner.TagIDs = append(banner.TagIDs, tagID)
		Banners = append(Banners, banner)
	}
	return Banners, nil
}

const numFieldForBulk = 3

func (repo *BannersSQL) bulkInsert(tagIDs []int, bannerID int, featureID int) (string, []interface{}) {
	valueStrings := make([]string, 0, len(tagIDs))
	valueArgs := make([]interface{}, 0, len(tagIDs)*numFieldForBulk)
	i := 0
	for _, tagID := range tagIDs {
		valueStrings = append(valueStrings, fmt.Sprintf("($%d, $%d, $%d)",
			i*numFieldForBulk+1, i*numFieldForBulk+2, i*numFieldForBulk+3))
		valueArgs = append(valueArgs, tagID)
		valueArgs = append(valueArgs, bannerID)
		valueArgs = append(valueArgs, featureID)
	}
	stmt := fmt.Sprintf(insertNewLog,
		strings.Join(valueStrings, ","))
	return stmt, valueArgs
}
