package delivery

import (
	"net/http"

	"banner_service/internal/middleware"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

func MapRoutes(handler Handlers) *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/user_banner", handler.GetBannerForUser).Methods("GET")
	r.HandleFunc("/banner", handler.FilterBanners).Methods("GET")
	r.HandleFunc("/banner", handler.CreateBanner).Methods("POST")
	r.HandleFunc("/banner/{id}", handler.PatchBanner).Methods("PATCH")
	r.HandleFunc("/banner/{id}", handler.DeleteBanner).Methods("DELETE")
	return r
}

func MapMiddlewares(r *mux.Router, logger *zap.SugaredLogger) http.Handler {
	r.Use(middleware.Auth)
	r.Use(middleware.AccessLog(logger))
	r.Use(middleware.Panic)
	return r
}
