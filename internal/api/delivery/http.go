package delivery

import (
	"banner_service/internal/api/usecase"
	"banner_service/pkg/myerrors"
	"banner_service/pkg/token"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"

	banners "banner_service/internal/api"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

type Handlers struct {
	Logger         *zap.SugaredLogger
	BannersUseCase usecase.BannersUseCase
}

func (h *Handlers) GetBannerForUser(w http.ResponseWriter, r *http.Request) {
	role, err := token.RoleFromContext(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if *role != token.User && *role != token.Admin {
		w.WriteHeader(http.StatusForbidden)
	}

	tagIDStr := r.URL.Query().Get("tag_id")
	tagID, err := strconv.Atoi(tagIDStr)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusBadRequest)
		return
	}
	featureIDStr := r.URL.Query().Get("feature_id")
	featureID, err := strconv.Atoi(featureIDStr)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusBadRequest)
		return
	}

	var useLastRevision bool
	useLastRevisionStr := r.URL.Query().Get("use_last_revision")
	if useLastRevisionStr != "" {
		useLastRevision, err = strconv.ParseBool(featureIDStr)
		if err != nil {
			w.Header().Set("Content-Type", "application/json")
			http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusBadRequest)
			return
		}
	}

	content, err := h.BannersUseCase.GetBannerForUser(r.Context(), tagID, featureID, useLastRevision, *role)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			w.WriteHeader(http.StatusForbidden)
		}
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusInternalServerError)
		return
	}
	if myerrors.HandleMarshalError(w, content) {
		h.Logger.Infof("Get banner: %v for user with tagID: %v, featureID: %v", content, tagID, featureID)
	} else {
		h.Logger.Infof("error at HandleMarshalError delivery.GetBannerForUser")
	}
}

func (h *Handlers) FilterBanners(w http.ResponseWriter, r *http.Request) {
	role, err := token.RoleFromContext(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if *role != token.Admin {
		w.WriteHeader(http.StatusForbidden)
	}
	tagID := r.URL.Query().Get("tag_id")
	if tagID != "" {
		_, err = strconv.Atoi(tagID)
		if err != nil {
			w.Header().Add("Content-Type", "application/json")
			http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusBadRequest)
			return
		}
	}

	featureID := r.URL.Query().Get("feature_id")
	if featureID != "" {
		_, err = strconv.Atoi(featureID)
		if err != nil {
			w.Header().Add("Content-Type", "application/json")
			http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusBadRequest)
			return
		}
	}

	offset := r.URL.Query().Get("offset")
	if offset != "" {
		_, err = strconv.Atoi(offset)
		if err != nil {
			w.Header().Add("Content-Type", "application/json")
			http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusBadRequest)
			return
		}
	}

	limit := r.URL.Query().Get("limit")
	if limit != "" {
		_, err = strconv.Atoi(limit)
		if err != nil {
			w.Header().Add("Content-Type", "application/json")
			http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusBadRequest)
			return
		}
	}

	Banners, err := h.BannersUseCase.FilterBanners(r.Context(), tagID, featureID, offset, limit)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusInternalServerError)
		return
	}
	if myerrors.HandleMarshalError(w, Banners) {
		h.Logger.Infof("Get banners: %v for user with tagID: %v, featureID: %v, offset: %v, limit: %v",
			Banners, tagID, featureID, offset, limit)
	} else {
		h.Logger.Infof("error at HandleMarshalError delivery.FilterBanners")
	}
}

func (h *Handlers) CreateBanner(w http.ResponseWriter, r *http.Request) {
	role, err := token.RoleFromContext(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if *role != token.Admin {
		w.WriteHeader(http.StatusForbidden)
	}
	banner := banners.Banner{}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusBadRequest)
	}
	err = json.Unmarshal(body, &banner)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusBadRequest)
	}
	bannerID, err := h.BannersUseCase.CreateBanner(r.Context(), banner)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
	if myerrors.HandleMarshalError(w, &banner) {
		h.Logger.Infof("Create banner: %v with bannerID: %v", banner, bannerID)
	} else {
		h.Logger.Infof("error at HandleMarshalError delivery.CreateBanner")
	}
}

func (h *Handlers) PatchBanner(w http.ResponseWriter, r *http.Request) {
	role, err := token.RoleFromContext(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if *role != token.Admin {
		w.WriteHeader(http.StatusForbidden)
	}
	contentType := r.Header.Get("Content-Type")
	if contentType != "application/json" {
		w.WriteHeader(http.StatusBadRequest)
	}
	idStr, ok := mux.Vars(r)["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
	}
	id, err := strconv.Atoi(idStr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	banner := banners.Banner{}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusBadRequest)
	}
	err = json.Unmarshal(body, &banner)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusBadRequest)
	}
	isValid, err := govalidator.ValidateStruct(banner)
	if err != nil || !isValid {
		http.Error(w, `{"error": "invalid initialize of struct"}`, http.StatusBadRequest)
	}

	err = h.BannersUseCase.PatchBanner(r.Context(), banner, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			w.WriteHeader(http.StatusNotFound)
		}
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	h.Logger.Infof("Patch banner %v with id: %v", banner, id)
}

func (h *Handlers) DeleteBanner(w http.ResponseWriter, r *http.Request) {
	role, err := token.RoleFromContext(r.Context())
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if *role != token.Admin {
		w.WriteHeader(http.StatusForbidden)
	}

	idStr, ok := mux.Vars(r)["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
	}
	id, err := strconv.Atoi(idStr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	err = h.BannersUseCase.DeleteBanner(r.Context(), id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			w.WriteHeader(http.StatusNotFound)
		}
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, fmt.Sprintf(`{"error": "%v"}`, err), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
	h.Logger.Infof("banner with id %v was deleted", id)
}
