package usecase

import (
	"banner_service/internal/api/repository"
	"banner_service/pkg/myerrors"
	"banner_service/pkg/token"
	"context"
	"log"
	"time"

	banners "banner_service/internal/api"

	"github.com/pkg/errors"
)

type BannersUseCase interface {
	GetBannerForUser(ctx context.Context, tagID, featureID int, useLastRevision bool, role string) (banners.Content, error)
	FilterBanners(ctx context.Context, tagID, featureID, offset, limit string) ([]banners.Banner, error)
	CreateBanner(ctx context.Context, banner banners.Banner) (int, error)
	PatchBanner(ctx context.Context, banner banners.Banner, bannerID int) error
	DeleteBanner(ctx context.Context, bannerID int) error
}

type bannersUseCase struct {
	sqlRepo   repository.BannersRepo
	cacheRepo repository.CacheRepo
}

const CacheUptime = 5 * time.Minute

func (uc *bannersUseCase) CacheRenew(ctx context.Context) {
	ticker := time.NewTicker(CacheUptime)
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			updatedBanners, err := uc.sqlRepo.GetLastUpdatedBanners(ctx)
			if err != nil {
				log.Println(err)
			}
			for _, banner := range updatedBanners {
				tagID := banner.TagIDs[0]
				featureID := banner.FeatureID
				switch banner.Status {
				case "deleted":
					err = uc.cacheRepo.RemoveBannerFromCache(tagID, featureID)
					if err != nil {
						log.Printf("failed to remove banner from cache: %v\n", err)
					}
				case "updated":
					uc.cacheRepo.AddOrUpdateBannerToCache(tagID, featureID, banner.Content)
				case "created":
					uc.cacheRepo.AddOrUpdateBannerToCache(tagID, featureID, banner.Content)
				}
			}
		}
	}
}

func NewBannersUseCase(ctx context.Context, sql repository.BannersRepo, cache repository.CacheRepo) *bannersUseCase {
	bannersUC := &bannersUseCase{
		sqlRepo:   sql,
		cacheRepo: cache,
	}
	go bannersUC.CacheRenew(ctx)
	return bannersUC
}

func (uc *bannersUseCase) GetBannerForUser(ctx context.Context,
	tagID, featureID int, useLastRevision bool, role string,
) (banners.Content, error) {
	var banner banners.Content
	var err error
	switch role {
	case token.Admin:
		if useLastRevision {
			return uc.sqlRepo.GetBannerForAdmin(ctx, tagID, featureID)
		}
		banner, err = uc.cacheRepo.GetBannerForUser(tagID, featureID)
		if err != nil {
			banner, err = uc.sqlRepo.GetBannerForAdmin(ctx, tagID, featureID)
		} else {
			uc.cacheRepo.AddOrUpdateBannerToCache(tagID, featureID, banner)
		}
	case token.User:
		if useLastRevision {
			return uc.sqlRepo.GetBannerForUser(ctx, tagID, featureID)
		}
		banner, err = uc.cacheRepo.GetBannerForUser(tagID, featureID)
		if err != nil {
			banner, err = uc.sqlRepo.GetBannerForUser(ctx, tagID, featureID)
		} else {
			uc.cacheRepo.AddOrUpdateBannerToCache(tagID, featureID, banner)
		}
	default:
		return banners.Content{}, myerrors.ErrInvalidRole
	}

	return banner, err
}

func (uc *bannersUseCase) FilterBanners(ctx context.Context,
	tagID, featureID, offset, limit string,
) ([]banners.Banner, error) {
	return uc.sqlRepo.FilterBanners(ctx, tagID, featureID, offset, limit)
}

func (uc *bannersUseCase) CreateBanner(ctx context.Context, banner banners.Banner) (int, error) {
	return uc.sqlRepo.CreateBanner(ctx, banner)
}

func (uc *bannersUseCase) PatchBanner(ctx context.Context, banner banners.Banner, bannerID int) error {
	return errors.Wrap(uc.sqlRepo.PatchBanner(ctx, banner, bannerID), "bannersUseCase.PatchBanner")
}

func (uc *bannersUseCase) DeleteBanner(ctx context.Context, bannerID int) error {
	return errors.Wrap(uc.sqlRepo.DeleteBanner(ctx, bannerID), "bannersUseCase.DeleteBanner")
}
