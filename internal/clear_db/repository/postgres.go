package repository

import (
	"context"
	"database/sql"
)

type GarbageDemon interface {
	DeleteOldBanners(ctx context.Context) error
}

type Postgres struct {
	db *sql.DB
}

func NewPostgresRepo(db *sql.DB) *Postgres {
	return &Postgres{db: db}
}

func (p *Postgres) DeleteOldBanners(ctx context.Context) error {
	_, err := p.db.ExecContext(ctx, DeleteOldBanners)
	return err
}
