package repository

var DeleteOldBanners = `
DELETE FROM log WHERE updated_at <= NOW() - INTERVAL '10 minutes' AND status = 'deleted';
`
