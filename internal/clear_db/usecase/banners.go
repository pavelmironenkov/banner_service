package usecase

import (
	"context"
	"log"
	"time"

	"banner_service/internal/clear_db/repository"
)

type GarbageDemonUseCase interface {
	DeleteOldBanners(ctx context.Context) error
}

type garbageDemonUseCase struct {
	repo repository.GarbageDemon
}

func NewGarbageDemonUseCase(repo repository.GarbageDemon) *garbageDemonUseCase {
	return &garbageDemonUseCase{repo: repo}
}

const GarbageCollectionUptime = 10 * time.Minute

func (uc *garbageDemonUseCase) DeleteOldBanners(ctx context.Context) error {
	ticker := time.NewTicker(GarbageCollectionUptime)
	for {
		select {
		case <-ctx.Done():
			return nil
		case <-ticker.C:
			err := uc.repo.DeleteOldBanners(ctx)
			if err != nil {
				log.Printf("failed to delete old banners: %v", err)
			}
		}
	}
}
