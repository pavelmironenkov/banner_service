CREATE TABLE IF NOT EXISTS feature(
    feature_id SERIAL PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS log(
    banner_id INT NOT NULL,
    tag_id INT NOT NULL,
    feature_id INT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now(),
    status varchar(10) NOT NULL DEFAULT 'created',
    FOREIGN KEY (banner_id) REFERENCES banners(banner_id),
    FOREIGN KEY (tag_id) REFERENCES tag(tag_id),
    FOREIGN KEY (feature_id) REFERENCES feature(feature_id),
    UNIQUE (banner_id, feature_id, tag_id),
    PRIMARY KEY (tag_id, feature_id)
);

CREATE TABLE IF NOT EXISTS tag(
    tag_id SERIAL PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS banners(
    banner_id SERIAL PRIMARY KEY,
    title VARCHAR(70) NOT NULL,
    text TEXT NOT NULL,
    url VARCHAR(2048) NOT NULL,
    is_active BOOLEAN NOT NULL DEFAULT true
);

CREATE INDEX updated_at ON log (updated_at);

CLUSTER log USING updated_at;

CREATE OR REPLACE FUNCTION set_updated_at() RETURNS TRIGGER AS
$set_updated_at$
BEGIN
    new.updated_at = now();
    RETURN new;
END;
$set_updated_at$ LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER set_log_updated_at
    BEFORE UPDATE
    ON log
    FOR EACH ROW
EXECUTE FUNCTION set_updated_at();