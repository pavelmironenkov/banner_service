package middleware

import (
	"net/http"

	"banner_service/pkg/token"
)

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("token")
		if tokenString == "" {
			next.ServeHTTP(w, r)
			return
		}
		role, err := token.Check(tokenString)
		if role != nil && err == nil {
			ctx := token.ContextWithRole(r.Context(), *role)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
