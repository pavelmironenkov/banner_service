FROM golang:alpine

COPY . /go/src/banner_service

WORKDIR /go/src/banner_service

RUN apk add make && apk add git && make build

EXPOSE 8080/tcp

CMD [ "/go/src/banner_service/api" ]

# docker build --file=Dockerfile --tag=crudapp:latest .
# docker run -d --rm -p 8080:8080 crudapp
# docker ps
# docker logs ...
