.PHONY: build
build:
	go build -mod=vendor -o /go/src/banner_service ./cmd/api/...
.PHONY: docker
docker:
	docker build -t pmironenkov/banner_service:latest .
	docker push pmironenkov/banner_service:latest
.PHONY: test
test:
	go test -v ./... -coverprofile=cover.out
	rm cover.out
.PHONY: lint
lint:
	golangci-lint -v --modules-download-mode=vendor -c ./.golangci.yml run --fix ./...