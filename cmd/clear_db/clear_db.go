package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"banner_service/internal/clear_db/repository"
	"banner_service/internal/clear_db/usecase"
)

const (
	host       = "localhost"
	port       = 5432
	usr        = "postgres"
	password   = "postgres"
	dbname     = "banner_service"
	dbConnWait = 5
	dbConnNum  = 100
)

func dbInit() *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, usr, password, dbname)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	db.SetMaxOpenConns(dbConnNum)

	err = db.Ping()
	for err != nil {
		err = db.Ping()
		time.Sleep(dbConnWait * time.Second)
	}
	return db
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	db := dbInit()
	bannersSQLRepo := repository.NewPostgresRepo(db)
	bannersUseCase := usecase.NewGarbageDemonUseCase(bannersSQLRepo)
	err := bannersUseCase.DeleteOldBanners(ctx)
	if err != nil {
		log.Println(err)
		return
	}
}
