package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"time"

	banners "banner_service/internal/api"
	"banner_service/internal/api/delivery"
	"banner_service/internal/api/repository"
	"banner_service/internal/api/usecase"
	lru "github.com/hashicorp/golang-lru/v2"
	_ "github.com/lib/pq"
	"go.uber.org/zap"
)

const (
	host       = "localhost"
	port       = 5432
	usr        = "postgres"
	password   = "postgres"
	dbname     = "banner_service"
	dbConnWait = 5 * time.Second
	dbConnNum  = 100
)

func dbInit() *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, usr, password, dbname)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	db.SetMaxOpenConns(dbConnNum)

	err = db.Ping()
	for err != nil {
		err = db.Ping()
		time.Sleep(dbConnWait)
	}
	return db
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	db := dbInit()
	zapLogger, err := zap.NewProduction()
	if err != nil {
		fmt.Println(err)
	}
	defer func() {
		err = zapLogger.Sync()
		if err != nil {
			fmt.Println(err)
		}
	}()

	logger := zapLogger.Sugar()

	cache, err := lru.New[repository.KeyRepositoryCache, banners.Content](repository.CacheSize)
	if err != nil {
		log.Printf("Cache is not created: %v", err)
		return
	}

	bannersSQLRepo := repository.NewPostgresRepo(db)
	bannersCacheRepo := repository.NewCache(cache)
	bannersUseCase := usecase.NewBannersUseCase(ctx, bannersSQLRepo, bannersCacheRepo)
	userHandler := delivery.Handlers{
		BannersUseCase: bannersUseCase,
		Logger:         logger,
	}

	handlersMux := delivery.MapRoutes(userHandler)
	middlewareMux := delivery.MapMiddlewares(handlersMux, logger)
	addr := ":8080"
	logger.Infow("starting api",
		"type", "START",
		"addr", addr,
	)
	err = http.ListenAndServe(addr, middlewareMux)
	if err != nil {
		logger.Error("Fail ListenAndServe with err: %v", err)
	}
}
